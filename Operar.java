package source;
import static source.Main.pubx;
import static source.Main.puby;
import static source.Main.pubop;
import static source.Main.pubresult;
public class Operar {
    public void opera(String x1, String y1, String op1){
        pubx = x1;
        puby = y1;
        pubop = op1;
//        System.out.println(pubresult);
    }
    public static void calcula(){
        double px = Double.parseDouble(pubx);
        double py = Double.parseDouble(puby);
        switch (pubop) {
            case "+":
                pubresult = "El resultado es: " + String.valueOf(px + py);
                System.out.println(pubresult);
                break;
            case "-":
                pubresult = "El resultado es: " + String.valueOf(px - py);
                System.out.println(pubresult);
                break;
            case "*":
                pubresult = "El resultado es: " + String.valueOf(px * py);
                System.out.println(pubresult);
                break;
            case "/":
                if (py != 0.0) {
                    pubresult = "El resultado es: " + String.valueOf(px + py);
                    System.out.println(pubresult);
                } else {
                    pubresult = "No se divide entre 0  porque el resultado es Infinito!";
                    System.out.println(pubresult);
                }
                break;
        }
    }
}
